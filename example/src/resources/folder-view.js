function switchFile() {
    const showClass = 'show';
    const activeClass = 'active';
    const folderClass = 'folder-viewer';
    const codeTag = 'div';
    const liTag = 'li';
    const pathAttribute = 'data-path';

    let folder = this.parentElement;
    while (!folder.classList.contains(folderClass)) {
        folder = folder.parentElement;
    }

    for (let li of folder.querySelectorAll(`${liTag}.${activeClass}`)) {
        if (li != this) {
            li.classList.remove(activeClass);
        }
    }
    this.classList.add(activeClass);

    let shown = folder.querySelector(`${codeTag}.${showClass}`);
    let target = folder.querySelector(`${codeTag}[${pathAttribute}="${this.getAttribute(pathAttribute)}"]`);
    if (target) {
        target.classList.add(showClass);
    }

    if (target !== shown && shown) {
        shown.classList.remove(showClass);
    }
}

let elements = document.querySelectorAll('.folder-viewer .folder-viewer-sidebar li.file');
for (let el of elements) {
    el.onclick = switchFile;
}
