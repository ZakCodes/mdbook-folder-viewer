extern crate base64;
extern crate glob;
extern crate mdbook;
extern crate pulldown_cmark;
extern crate pulldown_cmark_to_cmark;
extern crate serde;
extern crate serde_derive;
extern crate toml;
extern crate walkdir;
#[macro_use]
extern crate typed_html;

mod config;

use crate::config::*;
use mdbook::book::{Book, Chapter};
use mdbook::errors::{Error, Result};
use mdbook::preprocess::{Preprocessor, PreprocessorContext};
use mdbook::{BookItem, Config};
use pulldown_cmark::{CowStr, Event, Parser, Tag};
use pulldown_cmark_to_cmark::fmt::cmark;
use std::collections::HashMap;
use std::convert::TryInto;
use std::ffi::OsString;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use typed_html::elements::{div, li};

#[derive(Copy, Clone, Default)]
pub struct FolderViewer;

#[derive(Clone, Debug)]
pub enum Entry {
    File(String),
    Dir(String, Entries),
}
type Entries = HashMap<OsString, Entry>;

impl FolderViewer {
    pub fn process_chapter(&self, chapter: &mut Chapter, config: &Config) -> Result<()> {
        let mut skip_next = false;
        let events = Parser::new(&chapter.content).filter_map(|event| match event {
            Event::Start(Tag::CodeBlock(block)) => {
                if block.as_ref() == self.name() {
                    skip_next = true;
                    Some(Event::HardBreak)
                } else {
                    Some(Event::Start(Tag::CodeBlock(block)))
                }
            }
            Event::Text(block) if skip_next => {
                if skip_next {
                    let folder_config = self.read_config(&block);
                    let rendered = self.render_folder(&chapter.path, &folder_config, config);
                    Some(Event::Html(CowStr::from(rendered.to_string())))
                } else {
                    Some(Event::Text(block))
                }
            }
            Event::End(Tag::CodeBlock(block)) if skip_next => {
                if skip_next {
                    skip_next = false;
                    None
                } else {
                    Some(Event::End(Tag::CodeBlock(block)))
                }
            }
            event => Some(event),
        });

        let mut buf = String::with_capacity(chapter.content.len());
        cmark(events, &mut buf, None)
            .map(|_| {
                chapter.content = buf;
            })
            .map_err(|err| Error::from(format!("Markdown serialization failed: {}", err)))
    }

    pub fn read_config(&self, config: &str) -> FolderConfig {
        toml::from_str(config).expect("Folder configuration couldn't be parsed")
    }

    pub fn render_folder(
        &self,
        file_path: &PathBuf,
        folder_config: &FolderConfig,
        config: &Config,
    ) -> Box<div<String>> {
        let root = folder_config.path.clone().unwrap_or_else(|| {
            let mut root = file_path.clone();
            root.pop();
            root
        });
        let root = config.book.src.join(root);

        let mut entries: Entries = Entries::new();

        for entry in walkdir::WalkDir::new(root.as_path())
            .min_depth(1)
            .into_iter()
            .filter_map(|res| res.ok().filter(|entry| entry.file_type().is_file()))
        {
            let pathbuf = entry
                .path()
                .to_path_buf()
                .strip_prefix(root.as_path())
                .map(|path| path.to_path_buf())
                .unwrap();

            if folder_config
                .ignore
                .iter()
                .all(|pat| !pat.matches_path(pathbuf.as_path()))
                || folder_config
                    .include
                    .iter()
                    .any(|pat| pat.matches_path(pathbuf.as_path()))
            {
                let name = entry.file_name().to_os_string();
                let path_str = pathbuf.to_str().unwrap().to_string();

                let mut sub_entries = &mut entries;
                let ancestors: Vec<_> = pathbuf.ancestors().skip(1).collect();
                for ancestor in ancestors.into_iter().rev() {
                    if let Some(name) = ancestor.components().last() {
                        let ancestor_name = name.as_os_str().to_os_string();

                        let sub_entry = sub_entries.entry(ancestor_name).or_insert_with(|| {
                            Entry::Dir(ancestor.to_str().unwrap().to_string(), Entries::new())
                        });
                        if let Entry::Dir(_, entries) = sub_entry {
                            sub_entries = entries;
                        } else {
                            unreachable!();
                        }
                    }
                }

                sub_entries.insert(name, Entry::File(path_str));
            }
        }

        html!(
            <div class="folder-viewer">
                <ul class="folder-viewer-sidebar">
                    { Self::sidebar(&entries) }
                </ul>
                <div class="folder-viewer-files">
                    { Self::files(root.as_path(), &entries) }
                </div>
            </div>
        )
    }

    #[allow(clippy::vec_box)]
    pub fn sidebar(entries: &Entries) -> Vec<Box<li<String>>> {
        entries.iter().map(Self::sidebar_entry).collect()
    }

    fn sidebar_entry((name, entry): (&OsString, &Entry)) -> Box<li<String>> {
        match entry {
            Entry::Dir(ref path, ref entries) => html!(
                <li class="folder" data-path={ path.clone() }>
                    { text!(name.to_str().unwrap()) }
                    <ul>{ Self::sidebar(entries) }</ul>
                </li>
            ),
            Entry::File(ref path) => html!(
                <li class="file" data-path={ path.clone() }>
                    { text!("{}", name.to_str().unwrap()) }
                </li>
            ),
        }
    }

    #[allow(clippy::vec_box)]
    pub fn files(root: &Path, entries: &Entries) -> Vec<Box<div<String>>> {
        entries
            .iter()
            .enumerate()
            .filter_map(|(idx, (_, entry))| {
                if let Entry::File(relative) = entry {
                    let path = root.join(relative);
                    let div = Self::render_entry(&path, relative, idx == 0);
                    Some(div)
                } else {
                    None
                }
            })
            .collect()
    }

    fn render_entry(path: &PathBuf, relative_path: &str, is_selected: bool) -> Box<div<String>> {
        let mut file = File::open(&path).unwrap();
        let mut content = String::new();
        file.read_to_string(&mut content).unwrap();

        let ext = path.extension().map_or("txt", |ext| ext.to_str().unwrap());
        let mut div = html!(
            <div data-path={ relative_path }>
                { unsafe_text!("\n\n~~~{}\n\
                {}\
                ~~~\n", ext, content) }
            </div>
        );

        if is_selected {
            div.attrs.class = Some("show".try_into().unwrap());
        }

        div
    }
}

impl Preprocessor for FolderViewer {
    fn name(&self) -> &str {
        "folder-viewer"
    }

    fn run(&self, ctx: &PreprocessorContext, mut book: Book) -> Result<Book> {
        book.for_each_mut(|item| {
            if let BookItem::Chapter(ref mut chapter) = item {
                self.process_chapter(chapter, &ctx.config).unwrap();
            }
        });

        Ok(book)
    }

    /// Returns `true` when the given renderer is `html` or `markdown`
    fn supports_renderer(&self, renderer: &str) -> bool {
        renderer == "html" || renderer == "markdown"
    }
}
