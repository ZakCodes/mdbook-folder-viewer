use glob::Pattern;
use serde::de::{self, SeqAccess, Unexpected, Visitor};
use serde::Deserializer;
use serde_derive::Deserialize;
use std::fmt;
use std::path::PathBuf;
use std::str::FromStr;

#[derive(Debug, Deserialize)]
pub struct FolderConfig {
    /// Path of the folder containing the files to show.
    /// By default, the folder containing the current file is used.
    pub path: Option<PathBuf>,
    /// Patterns of the files to ignore.
    /// By default, no files are ignored.
    #[serde(deserialize_with = "deserialize_pattern", default)]
    pub ignore: Vec<Pattern>,
    /// Patterns of the files to include. This overrides the ignored files.
    /// By default, no files are included.
    #[serde(deserialize_with = "deserialize_pattern", default)]
    pub include: Vec<Pattern>,
    // Show the current file or not.
    // By default, the current file is hidden.
    #[serde(default)]
    pub include_current: bool,
}

fn deserialize_pattern<'de, D>(de: D) -> std::result::Result<Vec<Pattern>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Copy, Clone)]
    struct StrVisitor;
    impl<'de> Visitor<'de> for StrVisitor {
        type Value = Pattern;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a unix pattern inside of a string")
        }

        fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Pattern::from_str(value).map_err(|_| E::invalid_value(Unexpected::Str(value), &self))
        }
    }

    struct ListVisitor;
    impl<'de> Visitor<'de> for ListVisitor {
        type Value = Vec<Pattern>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a unix pattern inside of a string")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: SeqAccess<'de>,
        {
            let visitor = StrVisitor;

            let mut vec = Vec::new();
            while let Some(element) = seq.next_element()? {
                vec.push(visitor.visit_str(element)?);
            }

            Ok(vec)
        }
    }

    de.deserialize_seq(ListVisitor)
}

impl Default for FolderConfig {
    fn default() -> Self {
        Self {
            path: None,
            ignore: Vec::new(),
            include: Vec::new(),
            include_current: false,
        }
    }
}
