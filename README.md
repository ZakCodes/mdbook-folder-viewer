# Folder viewer for mdbook
This project is an [mdbook](https://github.com/rust-lang/mdBook) preprocessor for the `html` backend.  
It allows you to show the content of a folder.

## Features
* Specify the path of the folder, the default path is the folder containing the file containing containing the viewer 
* Hide some files in the viewer
* Only include certain files, the included files override the hidden files
* Show the current file or not

## Projected features
* Renaming files and folders
* Make it editable
* Specify the type of files with specific patterns

## TODO
* Regarder linkcheck backend ou regarder un preprocessor autre
* For this thing to be completely standalone, mdbook needs to allow users to change the preprocessor's config
